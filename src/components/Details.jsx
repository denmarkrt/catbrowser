import axios from 'axios'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

function Details() {
	const { id } = useParams()
	const apiUrl = 'https://api.thecatapi.com/v1'
	const imageEndpoint = `${apiUrl}/images/${id}`
	const [imageDetail, setImageDetail] = useState({})

	useEffect(() => {
		axios.get(imageEndpoint)
			.then(resp => {
				const { breeds, url } = resp.data
				const { id, name, origin, temperament, description } = breeds[0]
				
				setImageDetail({id, name, origin, temperament, description, url})
			})
			.catch(err => {
				console.log(err)
			})
	}, [])

	return (
		<div className="Details">
			<div className="container">
				<div className="card">
					<div className="card-header">
						<a className="btn btn-primary" href={`/?breed=${imageDetail.id}`}>Back</a>
					</div>
					<img alt="" className="card-img" src={imageDetail.url} />
					<div className="card-body">
						<h4>{imageDetail.name}</h4>
						<h5>Origin: {imageDetail.origin}</h5>
						<h6>{imageDetail.temperament}</h6>
						<p>{imageDetail.description}</p>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Details