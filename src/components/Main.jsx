import { useEffect, useState } from 'react';
import { Button, Container, Row, Col, Form } from 'react-bootstrap';
import { useLocation } from 'react-router-dom'
import axios from 'axios'

function Main() {
	const [breeds, setBreeds] = useState([])
	const [images, setImages] = useState([])
	const [page, setPage] = useState(1)
	const [currentBreed, setCurrentBreed] = useState('')
	const [buttonText, setButtonText] = useState('Load more')
	const search = useLocation().search
	const urlBreed = new URLSearchParams(search).get('breed')
	const apiUrl = 'https://api.thecatapi.com/v1'
	const breedsEndpoint = apiUrl + '/breeds'
	const imagesEndpoint = apiUrl + '/images/search'

	const buildBreedList = (data) => {
		let breedArray = []

		for(let i=0; i<data.length; i++) {
			breedArray.push({
				id: data[i].id,
				name: data[i].name
			})
		}

		setBreeds(breedArray)
	}

	const callBreedEndpoint = () => {
		axios.get(breedsEndpoint)
			.then(resp => {
				buildBreedList(resp.data)
			})
			.catch(err => {
				console.log(err)
			})
	}

	const renderCatBreedSelectList = () => {
		return (
			<Form.Select onChange={(e) => handleBreedChange(e.target.value)} value={currentBreed || urlBreed || ''}>
				 <option value="">Select breed</option>
			{breeds.map((breed, i) => {
					return <option value={breed.id} key={i}>{breed.name}</option>
			})}
			</Form.Select>
		)
	}

	const renderForm = () => {
		return (
			<form className='form-group'>
				<Form.Label>Breed</Form.Label>
				{ renderCatBreedSelectList() }<br />
			</form>
		)
	}

	const handleBreedChange = (breed) => {
		if(breed !== '') {
			const queryStringParams = `?page=${page}&limit=10&breed_id=${breed}`
			callImagesEndpoint(queryStringParams)
			setCurrentBreed(breed)
		}else {
			setImages([])
		}
	}

	const handleLoadMore = () => {
		let nextPage = page + 1
		const queryStringParams = `?page=${nextPage}&limit=10&breed_id=${currentBreed}`
		callImagesEndpoint(queryStringParams, 1)
		setPage(nextPage)
	}

	const callImagesEndpoint = (queryStringParams, append=0) => {
		setButtonText('Loading cats...')
		axios.get(imagesEndpoint+queryStringParams)
		.then(resp => {
			buildImageList(resp.data, append)
			setButtonText('Load more')
		})
		.catch(err => {
			alert('Apologies, but we could not load new cats for you at this time. Miau!')
			console.log(err)
		})
	}

	const buildImageList = (data, append=0) => {
		let imageArray = []
		if(append) 
		 imageArray = images

		for(let i=0; i<data.length; i++) {
			imageArray.push({
				id: data[i].id,
				url: data[i].url,
				alt: data[i].alt_names
			})
		}

		setImages(imageArray)
	}

	const renderImages = () => {
		return (
			images.map((image, i) => {
				return (
					<div className="col-md-3 col-sm-6 col-12" key={i}>
							<div className="card">
								<img alt={image.alt} className="card-img-top" src={image.url} />
								<div className="card-body">
									<a className="btn btn-primary btn-block" href={`/${image.id}`}>View details</a>
								</div>
							</div>
					</div>
				)
			})
		)
	}

	useEffect(() => {
		callBreedEndpoint()

		if(urlBreed) {
			const queryStringParams = `?page=${page}&limit=10&breed_id=${urlBreed}`
			callImagesEndpoint(queryStringParams)
		}
	}, [])
	
	return (
		<Container>
			<Row>
				<Col>
					<div className="Main">
						<div className='row'>
							<h1>Cat Browser</h1>
							{ renderForm() }
						</div>
						<div className='row'>
							{ images.length ? renderImages() : <div className="col-sm-12">No cats available</div> }	
						</div>
						<br /><br />
						<Button variant="success" onClick={handleLoadMore} disabled={images.length ? false: true}>{ buttonText }</Button>
					</div>
				</Col>
			</Row>
		</Container>
	)
}

export default Main;
